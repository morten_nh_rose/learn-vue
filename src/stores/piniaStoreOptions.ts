import {defineStore} from "pinia";

export const optionsStore = defineStore("firstStore", {
  state: () => ({
    count: 0
  }),

  actions: {
    increment() {
      this.count++
    },

    decrement() {
      this.count--
    }
  },

  getters: {
    doubleCount(state) {
      return state.count * 2
    }
  }
})
