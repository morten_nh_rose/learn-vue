import {computed, reactive} from "vue";

export function simpleStore() {
  // STATE
  const state = reactive({
    count: 0
  })

  // ACTIONS
  const increment = () => state.count++;
  const decrement = () => state.count--;

  // GETTERS

  const doubleCount = computed(() => state.count * 2)

  return { state, doubleCount, increment, decrement };
}
