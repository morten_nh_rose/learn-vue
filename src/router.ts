import { createWebHistory, createRouter } from 'vue-router'

import HomeView from './views/HomeView.vue'
import MortenView from './views/MortenView.vue'
import NestedView from './views/NestedView.vue'
import ChildView from './views/ChildView.vue'

const routes = [
  { path: '/', component: HomeView, name: 'Home' },
  { path: '/morten', component: MortenView, name: 'Morten' },
  {
    path: '/nested',
    name: 'nested',
    component: NestedView,
    children: [
      {
        path: 'child-a',
        component: ChildView,
      }
    ]
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})


router.beforeEach(async (to, from) => {
  console.log(to)
})

export default router;
